<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Laravel\Passport\Client as OClient;

trait PassportSimplified {

    /**
     * Calls the /oauth/token route of Laravel Passport to fetch auth token and refresh token for the given Oauth Client
     * and username/password combination.
     *
     * @param OClient $oClient
     * @param $email
     * @param $password
     * @return mixed
     */
    public function getTokenAndRefreshToken(OClient $oClient, $email, $password)
    {
        $req = Request::create('/oauth/token', 'POST',[
            'grant_type' => 'password',
            'client_id' => $oClient->id,
            'client_secret' => $oClient->secret,
            'username' => $email,
            'password' => $password,
            'scope' => '',
        ]);

        $response = app()->handle($req);
        $response = json_decode($response->getContent(), true);

        return $response;
    }
}
