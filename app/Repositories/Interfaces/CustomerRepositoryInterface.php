<?php

namespace App\Repositories\Interfaces;

use App\Models\Customer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

interface CustomerRepositoryInterface
{
    /**
     * Returns Customer Model.
     *
     * @return Customer
     */
    public function model(): Customer;

    /**
     * Returns Customer with given id.
     *
     * @param int $id
     * @return Customer
     * @throws ModelNotFoundException
     */
    public function get(int $id): Customer;

    /**
     * Creates new Customer from the given attributes.
     *
     * @param array $attributes
     * @return Customer
     */
    public function store(array $attributes): Customer;

    /**
     * Updates the given Customer with given attributes.
     *
     * @param Customer $Customer
     * @param array $attributes
     * @return Customer
     */
    public function update(Customer $Customer, array $attributes): Customer;

    /**
     * Deletes the given Customer.
     *
     * @param Customer $Customer
     * @return bool|null
     * @throws Exception;
     */
    public function destroy(Customer $Customer);
}
