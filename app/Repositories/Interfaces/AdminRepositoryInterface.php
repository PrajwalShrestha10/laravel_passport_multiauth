<?php

namespace App\Repositories\Interfaces;

use App\Models\Admin;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

interface AdminRepositoryInterface
{
    /**
     * Returns Admin Model.
     *
     * @return Admin
     */
    public function model(): Admin;

    /**
     * Returns Admin with given id.
     *
     * @param int $id
     * @return Admin
     * @throws ModelNotFoundException
     */
    public function get(int $id): Admin;

    /**
     * Creates new Admin from the given attributes.
     *
     * @param array $attributes
     * @return Admin
     */
    public function store(array $attributes): Admin;

    /**
     * Updates the given Admin with given attributes.
     *
     * @param Admin $admin
     * @param array $attributes
     * @return Admin
     */
    public function update(Admin $admin, array $attributes): Admin;

    /**
     * Deletes the given Admin.
     *
     * @param Admin $admin
     * @return bool|null
     * @throws Exception;
     */
    public function destroy(Admin $admin);
}
