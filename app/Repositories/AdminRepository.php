<?php

namespace App\Repositories;

use App\Models\Admin;
use App\Repositories\Interfaces\AdminRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Exception;

class AdminRepository implements AdminRepositoryInterface
{
    private $model;

    /**
     * AdminRepository constructor.
     *
     * @param Admin $model
     */
    public function __construct(Admin $model)
    {
        $this->model = $model;
    }

    /**
     * Returns Admin Model.
     *
     * @return Admin
     */
    public function model(): Admin
    {
        return $this->model;
    }

    /**
     * Returns Admin with given id.
     *
     * @param int $id
     * @return Admin
     * @throws ModelNotFoundException
     */
    public function get(int $id): Admin
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Creates new Admin from the given attributes.
     *
     * @param array $attributes
     * @return Admin
     */
    public function store(array $attributes): Admin
    {
        // Hashing password.
        $attributes['password'] = Hash::make($attributes['password']);

        return $this->model->create($attributes);
    }

    /**
     * Updates the given Admin with given attributes.
     *
     * @param Admin $admin
     * @param array $attributes
     * @return Admin
     */
    public function update(Admin $admin, array $attributes): Admin
    {
        // If there is 'password' field in $attributes, hashing the password. If 'password' field is empty or null, removing
        // the 'password' field.
        if (array_key_exists($attributes, 'password')) {
            if ($attributes['password'] != null && $attributes['password'] != '') {
                $attributes['password'] = Hash::make($attributes['password']);

            } else {
                unset($attributes['password']);
            }
        }

        $admin->update($attributes);

        return $admin;
    }

    /**
     * Deletes the given Admin.
     *
     * @param Admin $admin
     * @return bool|null
     * @throws Exception;
     */
    public function destroy(Admin $admin)
    {
        return $admin->delete();
    }
}
