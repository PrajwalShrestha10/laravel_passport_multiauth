<?php

namespace App\Repositories;

use App\Models\Customer;
use App\Repositories\Interfaces\CustomerRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Exception;

class CustomerRepository implements CustomerRepositoryInterface
{
    private $model;

    /**
     * CustomerRepository constructor.
     *
     * @param Customer $model
     */
    public function __construct(Customer $model)
    {
        $this->model = $model;
    }

    /**
     * Returns Customer Model.
     *
     * @return Customer
     */
    public function model(): Customer
    {
        return $this->model;
    }

    /**
     * Returns Customer with given id.
     *
     * @param int $id
     * @return Customer
     * @throws ModelNotFoundException
     */
    public function get(int $id): Customer
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Creates new Customer from the given attributes.
     *
     * @param array $attributes
     * @return Customer
     */
    public function store(array $attributes): Customer
    {
        // Hashing password.
        $attributes['password'] = Hash::make($attributes['password']);

        return $this->model->create($attributes);
    }

    /**
     * Updates the given Customer with given attributes.
     *
     * @param Customer $Customer
     * @param array $attributes
     * @return Customer
     */
    public function update(Customer $Customer, array $attributes): Customer
    {
        // If there is 'password' field in $attributes, hashing the password. If 'password' field is empty or null, removing
        // the 'password' field.
        if (array_key_exists($attributes, 'password')) {
            if ($attributes['password'] != null && $attributes['password'] != '') {
                $attributes['password'] = Hash::make($attributes['password']);

            } else {
                unset($attributes['password']);
            }
        }

        $Customer->update($attributes);

        return $Customer;
    }

    /**
     * Deletes the given Customer.
     *
     * @param Customer $Customer
     * @return bool|null
     * @throws Exception;
     */
    public function destroy(Customer $Customer)
    {
        return $Customer->delete();
    }
}
