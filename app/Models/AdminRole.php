<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AdminRole extends Model
{
    /**
     * Relation with AdminPermission.
     *
     * @return BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(AdminPermission::class,'admin_roles_admin_permissions', 'admin_role_id', 'admin_permission_id');
    }

    /**
     * Relation with Admin.
     *
     * @return HasMany
     */
    public function admins()
    {
        return $this->hasMany(Admin::class, 'admin_role_id');
    }
}
