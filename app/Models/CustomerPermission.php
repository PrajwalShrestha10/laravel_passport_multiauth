<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class CustomerPermission extends Model
{
    /**
     * Relation with AdminRole.
     *
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(CustomerRole::class,'customer_roles_customer_permissions', 'customer_permission_id', 'customer_role_id');
    }
}
