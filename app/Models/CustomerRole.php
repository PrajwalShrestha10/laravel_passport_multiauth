<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class CustomerRole extends Model
{
    /**
     * Relation with AdminPermission.
     *
     * @return BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(CustomerPermission::class,'customer_roles_customer_permissions', 'customer_role_id', 'customer_permission_id');
    }
}
