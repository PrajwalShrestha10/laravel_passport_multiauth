<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\AdminRepositoryInterface;
use App\Traits\PassportSimplified;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Passport\Client as OClient;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminAuthController extends Controller
{
    use PassportSimplified;

    private $adminRepository;

    /**
     * AdminAuthController constructor.
     *
     * @param AdminRepositoryInterface $adminRepository
     */
    public function __construct(AdminRepositoryInterface $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    /**
     * Registers new admin.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function register(Request $request)
    {
        try {
            DB::beginTransaction();

            // Validating data.
            $attributes = $this->validate($request, [
                'name' => 'required|string|min:3|max:255',
                'email' => 'required|email|unique:admins,email',
                'password' => 'required|string|confirmed|min:5|max:255'
            ]);

            $admin = $this->adminRepository->store($attributes);

            DB::commit();

            return response()->json([
                "status" => 200,
                "message" => "Admin created successfully.",
                "data" => $admin
            ]);

        } catch (ValidationException $exception) {
            DB::rollback();
            return response()->json([
                'status' => 422,
                'message' => $exception->getMessage(),
                'errors' => $exception->errors()
            ], 422);

        } catch (Exception $exception) {
            DB::rollback();
            throw $exception;
        }
    }

    /**
     * Logs in Admin with email and password.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function login(Request $request)
    {
        DB::beginTransaction();

        try {
            // Validating data.
            $attributes = $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required|string'
            ]);

            // Throwing error if the user with the given email doesn't exists or, if the password doesn't match.
            $admin = $this->adminRepository->model()->where('email', $attributes['email'])->first();
            if (!$admin || !Hash::check($attributes['password'], $admin->password)) {
                throw new AuthenticationException('Invalid email or password.');
            }

            // Fetching Passport Grant Client for Customer
            $oClient = OClient::where('password_client', 1)
                ->where('secret', '6wJS6hGuYGTs6ZTTT55Te4LWcoBgQcx6oR7pxkSb')
                ->first();

            // Fetching auth token and refresh token.
            $response = $this->getTokenAndRefreshToken($oClient, $attributes['email'], $attributes['password']);

            // Adding customer information in response.
            $response['admin'] = $admin;

            DB::commit();

            return response()->json([
                'status' => 200,
                'message' => 'Admin logged in successfully',
                'data' => $response
            ], 200);


        } catch (ValidationException $exception) {
            DB::rollBack();
            return response()->json([
                'status' => 422,
                'message' => $exception->getMessage(),
                'errors' => $exception->errors()
            ], 422);

        } catch (AuthenticationException $exception) {
            DB::rollBack();
            return response()->json([
                'status' => 401,
                'message' => $exception->getMessage()
            ], 401);

        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

    }

    /**
     * Fetches the logged in Admin.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        $admin = Auth::user();

        return response()->json([
            'status' => 200,
            'message' => 'Admin profile fetched successfully.',
            'data' => $admin
        ]);
    }
}
