<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Traits\PassportSimplified;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Passport\Client as OClient;

class CustomerAuthController extends Controller
{
    private $customerRepository;

    use PassportSimplified;

    /**
     * CustomerAuthController constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Registers new customer.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function register(Request $request)
    {
        try {
            DB::beginTransaction();

            // Validating data.
            $attributes = $this->validate($request, [
                'name' => 'required|string|min:3|max:255',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|string|confirmed|min:5|max:255'
            ]);

            $customer = $this->customerRepository->store($attributes);

            DB::commit();

            return response()->json([
                "status" => 200,
                "message" => "Customer created successfully.",
                "data" => $customer
            ]);

        } catch (ValidationException $exception) {
            DB::rollback();
            return response()->json([
                'status' => 422,
                'message' => $exception->getMessage(),
                'errors' => $exception->errors()
            ], 422);

        } catch (Exception $exception) {
            DB::rollback();
            throw $exception;
        }
    }

    /**
     * Logs in Customer with email and password.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function login(Request $request)
    {
        DB::beginTransaction();

        try {
            // Validating data.
            $attributes = $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required|string'
            ]);

            // Throwing error if the user with the given email doesn't exists or, if the password doesn't match.
            $customer = $this->customerRepository->model()->where('email', $request->email)->first();
            if (!$customer || !Hash::check($attributes['password'], $customer->password)) {
                throw new AuthenticationException('Invalid email or password.');
            }

            // Fetching Passport Grant Client for Customer
            $oClient = OClient::where('password_client', 1)
                ->where('secret', 'FEFxlGNQ8coj40ufZVfosHvh7gcw5zsnO0gD5cKM')
                ->first();

            // Fetching auth token and refresh token.
            $response = $this->getTokenAndRefreshToken($oClient, $attributes['email'], $attributes['password']);

            // Adding customer information in response.
            $response['customer'] = $customer;

            DB::commit();

            return response()->json([
                'status' => 200,
                'message' => 'Customer logged in successfully',
                'data' => $response
            ], 200);


        } catch (ValidationException $exception) {
            DB::rollBack();
            return response()->json([
                'status' => 422,
                'message' => $exception->getMessage(),
                'errors' => $exception->errors()
            ], 422);

        } catch (AuthenticationException $exception) {
            DB::rollBack();
            return response()->json([
                'status' => 401,
                'message' => $exception->getMessage()
            ], 401);

        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

    }

    /**
     * Fetches the logged in Customer.
     *
     * @return JsonResponse
     */
    public function profile()
    {
        $customer = Auth::user();

        return response()->json([
            'status' => 200,
            'message' => 'Customer profile fetched successfully.',
            'data' => $customer
        ]);
    }
}
