<?php

use App\Http\Controllers\API\AdminAuthController;
use App\Http\Controllers\API\CustomerAuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api-admin')->get('/user', function (Request $request) {
    return $request->user();
});


// Authentication Routes.
Route::group(['prefix' => 'auth/admin'], function () {
    Route::post("/register", [AdminAuthController::class, "register"]);
    Route::post("/login", [AdminAuthController::class, "login"]);
});

Route::group(['prefix' => 'auth/customer'], function () {
    Route::post("/register", [CustomerAuthController::class, "register"]);
    Route::post("/login", [CustomerAuthController::class, "login"]);
});


// Customer Routes.
Route::group(['prefix' => 'customer', 'middleware' => 'auth:api-customer'], function() {
    Route::get('/profile', [CustomerAuthController::class, 'profile']);
});

// Admin Routes.
Route::group(['prefix' => 'admin', 'middleware' => 'auth:api-admin'], function() {
    Route::get('/profile', [AdminAuthController::class, 'profile']);
});
