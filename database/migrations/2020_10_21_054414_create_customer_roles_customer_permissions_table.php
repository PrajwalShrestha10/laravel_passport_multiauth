<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerRolesCustomerPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_roles_customer_permissions', function (Blueprint $table) {
            $table->unsignedBigInteger('customer_role_id');
            $table->unsignedBigInteger('customer_permission_id');

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('customer_role_id')->references('id')->on('customer_roles')->onDelete('cascade');
            $table->foreign('customer_permission_id')->references('id')->on('customer_permissions')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            $table->primary(['customer_role_id','customer_permission_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_roles_customer_permissions');
    }
}
