<?php

use Illuminate\Database\Seeder;

class AdminRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdminRole = \App\Models\AdminRole::create([
            'name' => 'Super Admin',
            'slug' => 'super-admin'
        ]);

        $normalAdminRole = \App\Models\AdminRole::create([
            'name' => 'Normal Admin',
            'slug' => 'normal-admin'
        ]);

        $createAdminPermission = \App\Models\AdminPermission::create([
            'name' => 'Create Admin',
            'slug' => 'create-admin'
        ]);

        $deleteAdminPermission = \App\Models\AdminPermission::create([
            'name' => 'Delete Admin',
            'slug' => 'delete-admin'
        ]);

        $createCustomerPermission = \App\Models\AdminPermission::create([
            'name' => 'Create Customer',
            'slug' => 'create-customer'
        ]);

        $deleteCustomerPermission = \App\Models\AdminPermission::create([
            'name' => 'Delete Customer',
            'slug' => 'delete-customer'
        ]);

        $superAdminRole->admin_permissions()->sync([
            $createAdminPermission->id,
            $deleteAdminPermission->id,
            $createCustomerPermission->id,
            $deleteCustomerPermission->id
        ]);

        $normalAdminRole->admin_permissions()->sync([
            $createCustomerPermission->id,
            $deleteCustomerPermission->id
        ]);
    }
}
